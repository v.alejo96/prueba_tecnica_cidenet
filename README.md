# Cidenet Technical test

This repository contains all the code and documentation for the Cidenet technical test for the job as a Python
Bilingual Developer. 

Inside the ```rest-api-employees``` directory are all the code, documentation and other files for the installation,
deployment and setup of the solution. PLEASE, read all the README files BEFORE deploying.

For developing and testing locally, it is necessary to have installed PyMySQL (version 1.0.2) as dependency.
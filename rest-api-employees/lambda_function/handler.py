"""
This script makes queries to the employee RDS Database
"""

import os
import pymysql
import json
from controllers import create, read, update, delete

# Configuration Values
PROXY_ENDPOINT = os.environ['DB_HOST']
DB_NAME = os.environ['DB_NAME']
USERNAME = os.environ['DB_USER']  # FOR A PRODUCTION SCENARIO, THIS SHOULD BE RETRIEVED FROM AWS SECRETS MANAGER
PASSWORD = os.environ['DB_PASS']  # FOR A PRODUCTION SCENARIO, THIS SHOULD BE RETRIEVED FROM AWS SECRETS MANAGER


def get_request_params(event):
    """
    Method to extract params from the event data
    :param event: lambda event
    :type event: dict
    :return: Tuple of (true, event path,event type, event params) if the event contain them, else (False,"","","")
    :rtype: tuple
    """
    try:
        event_path = event.get("path").replace("/employees", "")
        event_type = event.get("httpMethod")
        query_params = event.get("queryStringParameters") if event.get("queryStringParameters") is not None else {}
        path_params = event.get("pathParameters") if event.get("pathParameters") is not None else {}
        body = json.loads(event.get("body")) if event.get("body") is not None else {}
        return False, event_path, event_type, query_params, path_params, body

    except Exception as e:
        print(e)
        return True, "", "", "", "", ""


def handler(event, context):
    """
    Lambda handler for the employee DB API. This method handles the different endpoints defined for the API
    :param event: Lambda event. the event must have a Restful format and contains the method type and the event path
    :type event: dict
    :param context: Lambda context, this element is currently unused
    :type context: dict
    :return: HTTP request response
    :rtype: dict
    """
    # Connection
    connection = pymysql.connect(host=PROXY_ENDPOINT,
                                 user=USERNAME,
                                 passwd=PASSWORD,
                                 db=DB_NAME,
                                 cursorclass=pymysql.cursors.DictCursor)

    # Get request data
    error, event_path, event_type, query_params, path_params, body = get_request_params(event)
    if error and event_path is not None:
        return {"statusCode": 401, "body": json.dumps({"message": "Incomplete params"})}

    # Redirect to appropriate controller
    if event_path == '/create' and event_type == 'POST':
        return create.insert(connection, body)
    if event_path == '/query' and event_type == 'GET':
        return read.select(connection, query_params)
    if event['resource'] == '/employees/{employee_id}' and event_type == 'PUT':
        return update.update(connection, path_params['employee_id'], body)
    if event['resource'] == '/employees/{employee_id}' and event_type == 'DELETE':
        return delete.delete(connection, path_params['employee_id'])
    else:
        return {"statusCode": 401, "body": json.dumps({"message": f"The endpoint {event_path} is not supported"})}

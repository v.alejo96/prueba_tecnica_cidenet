"""
Controller script for querying the database
"""
import datetime
import json


def select(connection, query_params):
    """
    This method queries the employee database with the given filters in the query parameters
    :param connection: Connection to the database
    :type: PyMySQL Connection Object
    :param query_params: Query parameters from the request
    :type query_params: dict
    :return: HTTP request response
    :rtype: dict
    """
    # Construct condition from query params
    sql_cond = 'WHERE ' if query_params else ''
    for key, value in query_params.items():
        sql_cond = sql_cond + f"{key} = '{value}' AND "
    sql_cond = sql_cond[:-5]

    # Make query
    cursor = connection.cursor()
    cursor.execute(f"""
    SELECT * 
    FROM employee
    {sql_cond}
    """)

    rows = cursor.fetchall()
    for row in rows:
        for key, value in row.items():
            if isinstance(value, datetime.date):
                row[key] = value.strftime("%d/%m/%Y")
            if isinstance(value, datetime.datetime):
                row[key] = value.strftime("%d/%m/%Y %H:%M:%S")

    # Close connection
    cursor.close()
    connection.close()

    if not rows:
        rows = "The query returned no results"
    return {"statusCode": 200, "body": json.dumps(rows)}

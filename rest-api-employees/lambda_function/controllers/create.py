"""
Controller script for the creation of new employees
"""
import json
import pymysql.err
from .utils import is_data_wrong, reformat_date_string


def insert(connection, body):
    """
    This generates the email and inserts a employee data into the database
    :param connection: Connection to the database
    :type: PyMySQL Connection Object
    :param body: Body from the request containing
    :type body: dict
    :return: HTTP request response
    :rtype: dict
    """
    if is_data_wrong(body):
        return {"statusCode": 406, "body": json.dumps({"message": "The employee data is not valid"})}
    body['entry_date'] = reformat_date_string(body['entry_date'])

    # Query for employees with same first name and last name for the creation of a unique email
    query_cursor = connection.cursor()
    query_cursor.execute(f"""
    SELECT first_name, last_name_1 
    FROM employee 
    WHERE first_name = %s AND last_name_1 = %s
    """, (body['first_name'], body['last_name_1']))

    rows = query_cursor.fetchall()
    query_cursor.close()
    email = f"{body['first_name']}.{body['last_name_1']}".lower()
    if len(rows) > 0:
        email = email + f'.{len(rows)}'
    if body['country'].upper() == 'USA':
        email = email + '@cidenet.com.us'
    else:
        email = email + '@cidenet.com.co'
    body.update({'email': email})

    # Insert all data
    columns = list(body.keys())
    values = list(body.values())
    list_to_str = ', '.join(map(str, columns))

    try:
        cursor = connection.cursor()
        cursor.execute(f"""
        INSERT INTO employee ({list_to_str}, registry_time)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, NOW())
        """, values)
    except pymysql.err.IntegrityError as e:
        print(e)
        connection.close()
        return {"statusCode": 409,
                "body": json.dumps({"message": "An employee with this ID already exists in the database"})}

    # Commit changes and close connection
    cursor.close()
    connection.commit()
    connection.close()
    return {"statusCode": 200, "body": json.dumps({"message": "New employee registered in database"})}

"""
Controller script for deleting an employee data
"""
import json


def delete(connection, employee_id):
    """
    This method updates the data for a given employee
    :param connection: Connection to the database
    :type: PyMySQL Connection Object
    :param employee_id: Unique identifier and primary key of the employee in the database
    :type employee_id: str
    :return: HTTP request response
    :rtype: dict
    """
    # Delete data
    cursor = connection.cursor()
    cursor.execute(f"""
    DELETE FROM employee
    WHERE employee_id = '{employee_id}'
    """)

    # Commit changes and close connection
    cursor.close()
    connection.commit()
    connection.close()
    return {"statusCode": 200, "body": json.dumps({"message": "Employee data deleted from database"})}

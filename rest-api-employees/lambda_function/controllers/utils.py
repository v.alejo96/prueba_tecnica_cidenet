import re
from datetime import datetime


def reformat_date_string(date_string):
    """
    Method that reformat a date string from DD/MM/YYYY to YYYY/MM/DD
    :param date_string: String to be reformatted
    :type date_string: dict
    :return: Reformatted string
    :rtype: str
    """
    return f'{date_string[6:]}/{date_string[3:5]}/{date_string[:2]}'


def is_data_wrong(body):
    """
    Method that checks if the data given complies with the table constraints
    :param body: Body from the HTTP request
    :type body: Dict
    :return: True if data does not complies, False otherwise
    :rtype: Bool
    """
    # Check that id, first names and last names don't have more than 20 characters and if other names more than 50
    if any(ele > 20 for ele in (len(body['employee_id']),
                                len(body['first_name']),
                                len(body['last_name_1']),
                                len(body['last_name_2']))) or len(body['other_names']) > 50:
        print('length')
        return True

    # Check if country is not different from COLOMBIA or USA
    if body['country'] not in ('COLOMBIA', 'USA'):
        print('country')
        return True

    # Check that id type is one of the given options
    if body['id_type'] not in ('CEDULA DE CIUDADANIA', 'CEDULA DE EXTRANJERIA', 'PASAPORTE', 'PERMISO ESPECIAL'):
        print('id type')
        return True

    # Check that area is one of the given options
    if body['area'] not in ('ADMINISTRACION', 'FINANCIERA', 'COMPRAS', 'INFRAESTRUCTURA', 'OPERACION', 'TALENTO HUMANO',
                            'SERVICIOS VARIOS'):
        print('area')
        return True

    # Check that work status is ACTIVE
    if body['work_status'] != 'ACTIVO':
        print('work status')
        return True

    # Check that id doesn't have any characters besides alphanumeric characters and hyphens, excluding
    if not bool(re.search(r'^[a-zA-Z0-9\-]+$', body['employee_id'])):
        print('employee id characters')
        return True

    # Check that first names and last names have only capitalized letters
    if any(not bool(re.search(r'^[A-Z]+$', ele)) for ele in (body['first_name'],
                                                             body['last_name_1'],
                                                             body['last_name_2'])):
        print('name characters')
        return True

    # Check that other names have only capitalized letters and blank spaces
    if not bool(re.search(r'^[A-Z\s]+$', body['other_names'])):
        print('other names characters')
        return True

    if not bool(re.search(r'^([0-3][0-9])(/)([0-1][0-9])(/)([0-9][0-9][0-9][0-9])+$', body['entry_date'])):
        print('entry date format')
        return True

    try:
        date = datetime.strptime(body['entry_date'], "%d/%m/%Y")
        if date > datetime.now():
            print('Entry date')
            return True
    except ValueError as e:
        print(e)
        return True

    return False

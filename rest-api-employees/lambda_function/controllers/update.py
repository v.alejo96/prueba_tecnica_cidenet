"""
Controller script for updating an employee data
"""
import json
import pymysql.err
from .utils import is_data_wrong, reformat_date_string


def update(connection, employee_id, body):
    """
    This method updates the data for a given employee
    :param connection: Connection to the database
    :type: PyMySQL Connection Object
    :param employee_id: Unique identifier and primary key of the employee in the database
    :type employee_id: str
    :param body: Body from the request containing the data
    :type body: dict
    :return: HTTP request response
    :rtype: dict
    """
    if is_data_wrong(body):
        return {"statusCode": 406, "body": json.dumps({"message": "The employee data is not valid"})}

    body['entry_date'] = reformat_date_string(body['entry_date'])

    sql_set = ''
    for key, value in body.items():
        sql_set = sql_set + f"{key} = '{value}', "
    sql_set = sql_set + "update_time = NOW()"

    # Update data
    try:
        cursor = connection.cursor()
        cursor.execute(f"""
        UPDATE employee
        SET {sql_set}
        WHERE employee_id = '{employee_id}'
        """)
    except pymysql.err.IntegrityError as e:
        print(e)
        connection.close()
        return {"statusCode": 409,
                "body": json.dumps({"message": "An employee with this ID already exists in the database"})}

    # Commit changes and close connection
    cursor.close()
    connection.commit()
    connection.close()
    return {"statusCode": 200, "body": json.dumps({"message": "Employee data updated in database"})}

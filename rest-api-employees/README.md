# Cidenet Technical Test: API for employee database

This service contains all the necessary resources (RDS Database, VPC and Proxy) and the API as the allows the CRUD 
operations to an RDS Database. The service is developed in Infrastructure as Code (IaC) defined in the `serverless.yml`
file that deploys two lambdas, one that creates the employee table and another one that allows for all CRUD operations 
to the database, the API Gateway, an RDS database, a VPC and RDS Proxy.

The API has endpoints for three endpoints defined.
- ``/employees/create``: For adding new employees to the database.
- ``/employees/query``: For making queries to the database.
- ``/employees/{employee_id}``: For updating or deleting a particular employee data.


## Installation

### Dependencies

To deploy the service the next dependencies are required:
- AWS CLI v2
- Docker
- Node.js
- npm
- Python 3.8+
- Serverless framework (to install use the command `npm install -g serverless`) 
- Install npm packages defined in the package.json via `npm install`
- Install the requirements defined in the requirements.txt file via `pip install -r requirements.txt`

### Configuration and Deployment

Open up a console and configure the AWS credentials given in ``test_credentials.csv`` by running the next command and 
set it as:
```bash
$ aws configure
AWS Access Key ID [None]: AKIAU2FAWKSQD5SIACW4
AWS Secret Access Key [None]: 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
Default region name [None]: us-east-1
Default output format [None]: json
```
This IAM user has all the necessary role policies to deploy and remove the service.

To deploy, open up a console over the ``rest-api-employees`` directory and enter the following command: 
``serverless deploy -v``. To change the stage, add the parameter ``--stage {stage}``. This process can take several 
minutes to complete.

To remove the stack run the command ``serverless remove -v``. This process can take several minutes.

### Setup Database

Invoke the lambda function that creates the table ``employee`` in the database with the next command:
```bash
$ aws lambda invoke --function-name prueba-cidenet-api-employees-create-employee-table-dev --payload "{}" response.json
```

## Testing

After deployment and setup, use the Jupyter Notebook ``user_journey_example.ipynb`` to test the different endpoints.

**IMPORTANT:** After deployment, the API URL is generated at random. This URL is used inside the notebook, so if the URL is
not up-to-date, end-to-end testing can't be done. To get the URL, look at the output after executing ``serverless
deploy -v`` and retrieve the API URL. For example, 
```bash
...
Serverless: Stack update finished...
Service Information
service: prueba-cidenet-api-employees
stage: dev
region: us-east-1
stack: prueba-cidenet-api-employees-dev
resources: 37
api keys:
  None
endpoints:
  POST - https://18fj311dy9.execute-api.us-east-1.amazonaws.com/dev/employees/create
  GET - https://18fj311dy9.execute-api.us-east-1.amazonaws.com/dev/employees/query
  PUT - https://18fj311dy9.execute-api.us-east-1.amazonaws.com/dev/employees/{employee_id}
  DELETE - https://18fj311dy9.execute-api.us-east-1.amazonaws.com/dev/employees/{employee_id}
functions:
  create-table: prueba-cidenet-api-employees-create-employee-table-dev
  query-employees: prueba-cidenet-api-employees-query-employees-dev
layers:
  None

Stack Outputs
CreateDashtableLambdaFunctionQualifiedArn: arn:aws:lambda:us-east-1:331049489568:function:prueba-cidenet-api-employees-create-employee-table-dev:4
QueryDashemployeesLambdaFunctionQualifiedArn: arn:aws:lambda:us-east-1:331049489568:function:prueba-cidenet-api-employees-query-employees-dev:41
ServiceEndpoint: https://18fj311dy9.execute-api.us-east-1.amazonaws.com/dev
ServerlessDeploymentBucketName: prueba-cidenet-api-emplo-serverlessdeploymentbuck-1tt6awkoux05i
...
```
The particular URL for this output is ``https://18fj311dy9.execute-api.us-east-1.amazonaws.com/dev/employees``.

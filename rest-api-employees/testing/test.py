"""
Unitary tests for handler functions
"""
import os
import json
import unittest

os.environ['DB_HOST'] = 'prp3r79c7qwl21.cpwgx9xejpjq.us-east-1.rds.amazonaws.com'
os.environ['DB_NAME'] = 'cidenetEmployeeDb'
os.environ['DB_USER'] = 'admin'
os.environ['DB_PASS'] = '12345678'

from lambda_function.handler import handler


class TestingAPI(unittest.TestCase):
    def test_create(self):
        """
        Test case for POST requests.
        """
        # A new employee registry is created in the database
        event = {"path": "/employees/create",
                 "httpMethod": "POST",
                 "pathParameters": {},
                 "headers": {},
                 "body": json.dumps({
                     'employee_id': '123456789',
                     'id_type': 'CEDULA DE CIUDADANIA',
                     'first_name': 'JUAN',
                     'other_names': 'CARLOS',
                     'last_name_1': 'MONTOYA',
                     'last_name_2': 'ALZATE',
                     'country': 'USA',
                     'entry_date': '24/10/2021',
                     'area': 'COMPRAS',
                     'work_status': 'ACTIVO'
                 })}
        response = handler(event, {})
        response['body'] = json.loads(response['body'])
        print(response)
        self.assertEqual('New employee registered in database', response['body']['message'])

        # Another employee with the same id is rejected to be registered in the database
        response = handler(event, {})
        response['body'] = json.loads(response['body'])
        print(response)
        self.assertEqual("An employee with this ID already exists in the database", response['body']['message'])

        # Some data doesn't comply to the table constraints
        event = {"path": "/employees/create",
                 "httpMethod": "POST",
                 "pathParameters": {},
                 "headers": {},
                 "body": json.dumps({
                     'employee_id': '12345678921365468746897465135246584658749687564',
                     'id_type': 'CEDULA DE CIUDADANIA',
                     'first_name': 'JUAN',
                     'other_names': 'CARLOS',
                     'last_name_1': 'MONTOYA',
                     'last_name_2': 'ALZATE',
                     'country': 'USA',
                     'entry_date': '29/09/2018',
                     'area': 'COMPRAS',
                     'work_status': 'ACTIVO'
                 })}
        response = handler(event, {})
        response['body'] = json.loads(response['body'])
        print(response)
        self.assertEqual("The employee data is not valid", response['body']['message'])

    def test_get(self):
        event = {"path": "/employees/query",
                 "httpMethod": "GET",
                 "pathParameters": {},
                 "headers": {},
                 "queryStringParameters": {
                 }}
        response = handler(event, {})
        response['body'] = json.loads(response['body'])
        print(response)

    def test_update(self):
        event = {"resource": "/employees/{employee_id}",
                 "path": "/employees/{employee_id}",
                 "httpMethod": "PUT",
                 "pathParameters": {'employee_id': '43524uyhgi87btfd'},
                 "headers": {},
                 "body": json.dumps({
                     'employee_id': '43524uyhgi87btfd',
                     'id_type': 'CEDULA DE EXTRANJERIA',
                     'first_name': 'JUAN',
                     'other_names': 'CARLOS',
                     'last_name_1': 'MONTOYA',
                     'last_name_2': 'ALZATE',
                     'country': 'COLOMBIA',
                     'entry_date': '29/06/2019',
                     'area': 'FINANCIERA',
                     'work_status': 'ACTIVO'
                 })}
        response = handler(event, {})
        response['body'] = json.loads(response['body'])
        print(response)
        self.assertEqual('Employee data updated in database', response['body']['message'])

    def test_delete(self):
        event = {"resource": "/employees/{employee_id}",
                 "path": "/employees/{employee_id}",
                 "httpMethod": "DELETE",
                 "pathParameters": {'employee_id': '123456789'},
                 "headers": {}
                 }
        response = handler(event, {})
        response['body'] = json.loads(response['body'])
        print(response)
        self.assertEqual('Employee data deleted from database', response['body']['message'])


if __name__ == '__main__':
    unittest.main()

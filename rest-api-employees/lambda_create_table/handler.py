"""
This script creates the employee table in the RDS Database
"""

import os
import pymysql
import json

PROXY_ENDPOINT = os.environ['DB_HOST']
DB_NAME = os.environ['DB_NAME']
USERNAME = os.environ['DB_USER']  # FOR A PRODUCTION SCENARIO, THIS SHOULD BE RETRIEVED FROM AWS SECRETS MANAGER
PASSWORD = os.environ['DB_PASS']  # FOR A PRODUCTION SCENARIO, THIS SHOULD BE RETRIEVED FROM AWS SECRETS MANAGER


def handler(event, context):
    connection = pymysql.connect(host=PROXY_ENDPOINT,
                                 user=USERNAME,
                                 passwd=PASSWORD,
                                 db=DB_NAME,
                                 cursorclass=pymysql.cursors.DictCursor)

    try:
        cursor = connection.cursor()
        cursor.execute("""
        CREATE TABLE employee (
        employee_id VARCHAR(20) PRIMARY KEY,
        id_type VARCHAR(30),
        first_name VARCHAR(20) NOT NULL,
        other_names VARCHAR(50),
        last_name_1 VARCHAR(20) NOT NULL,
        last_name_2 VARCHAR(20) NOT NULL,
        country VARCHAR(15) NOT NULL,
        email VARCHAR(300),
        entry_date DATE NOT NULL,
        area VARCHAR(30) NOT NULL,
        work_status VARCHAR(10) NOT NULL,
        registry_time TIMESTAMP,
        update_time TIMESTAMP,
        CHECK (country IN ('COLOMBIA', 'USA')),
        CHECK (id_type IN ('CEDULA DE CIUDADANIA', 'CEDULA DE EXTRANJERIA', 'PASAPORTE', 'PERMISO ESPECIAL')),
        CHECK (work_status = 'ACTIVO'),
        CHECK (area IN ('ADMINISTRACION', 'FINANCIERA', 'COMPRAS', 'INFRAESTRUCTURA', 'OPERACION', 'TALENTO HUMANO', 'SERVICIOS VARIOS'))
        )
        """)
    except pymysql.err.OperationalError as e:
        print(e)
        connection.close()
        return {"statusCode": 409, "body": json.dumps({"message": "Table already exists"})}

    cursor.close()
    connection.commit()
    connection.close()

    return {"statusCode": 200, "body": json.dumps({"message": "Table created successfully"})}
